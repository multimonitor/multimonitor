#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QDesktopWidget>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), desktopWidget( new QDesktopWidget()), offset(QPoint(0, 0)), moving(false)
{
    ui->setupUi(this);
    setMouseTracking(true);
    move(0, 0);

#ifdef Q_WS_X11
   setWindowFlags(Qt::Window | Qt::CustomizeWindowHint);

#else
   setWindowFlags(Qt::ToolTip);
#endif
    connect( ui->pushButton, SIGNAL(clicked()), qApp, SLOT(quit()) ); // QApplication::setQuitOnLastWindowClosed(true) is not working on Mac
    connect(desktopWidget, SIGNAL(screenCountChanged(int)), this, SLOT(updateLabels()));
    connect(desktopWidget, SIGNAL(workAreaResized(int)), this, SLOT(updateLabels()));
    QTimer::singleShot(0, this, SLOT(updateLabels()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::quit() {
    qApp->quit();
}

void MainWindow::updateLabels() {
    ui->screenCountLabel->setText(tr("There are %n screen(s)", "", desktopWidget->screenCount()));
    ui->screenNumberLabel->setText(tr("This widget is mostly in screen %1").arg(desktopWidget->screenNumber(this)));

    QString geometryText(tr("Available geometry:\n"));
    for( int i = 1; i <= desktopWidget->screenCount(); ++i ) {
        geometryText += QString(tr("       Screen %1: %2 x %3\n").arg(i).arg(1 + desktopWidget->screenGeometry(i).right() - desktopWidget->screenGeometry(i).left()).arg(1 + desktopWidget->screenGeometry(i).bottom() - desktopWidget->screenGeometry(i).top()));
    }
    ui->availableGeometryLabel->setText(geometryText);
}

void MainWindow::mousePressEvent(QMouseEvent *event) {

    if( (event->button() == Qt::LeftButton) ) {
        moving = true;
        offset = event->pos();
        qDebug("Moving = true, offset = (%d, %d)", offset.x(), offset.y() );
    }

    QMainWindow::mousePressEvent(event);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    qDebug("In MainWindow::mouseMoveEvent");
    if(moving) {
        this->move(event->globalPos() - offset);
        //ui->label->setText(tr("This is (%1, %2)").arg(this->pos().x()).arg(this->pos().y()));
        //ui->label->setText(tr("This is (%1, %2)").arg(event->x()).arg(event->y()));
        ui->positionLabel->setText(tr("This is (%1, %2)").arg(event->globalX()).arg(event->globalY()));
        //qDebug("Still moving, pos = (%d, %d), offset = (%d, %d)", this->pos().x(), this->pos().y(), offset.x(), offset.y() );
        //qDebug("Still moving, pos = (%d, %d), offset = (%d, %d)", event->x(), event->y(), offset.x(), offset.y() );
        //qDebug("Still moving, pos = (%d, %d), offset = (%d, %d)", event->pos().x(), event->pos().y(), offset.x(), offset.y() );
        qDebug("Still moving, pos = (%d, %d), offset = (%d, %d)", event->globalX(), event->globalY(), offset.x(), offset.y() );
        updateLabels();
    }

    QMainWindow::mouseMoveEvent(event);
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{

    if(event->button() == Qt::LeftButton)  {
        moving = false;
        qDebug("Moving = false");
    }

    QMainWindow::mouseReleaseEvent(event);
}
