#-------------------------------------------------
#
# Project created by QtCreator 2011-01-20T23:18:28
#
#-------------------------------------------------

QT       += core gui

TARGET = multimonitor
TEMPLATE = app

TRANSLATIONS += multimonitor_es.ts

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = 

symbian {
    TARGET.UID3 = 0xe0e6da1b
    # TARGET.CAPABILITY += 
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}

RESOURCES += \
    multimonitor.qrc

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/bin
    } else {
        target.path = /usr/local/bin
    }
    INSTALLS += target
}
